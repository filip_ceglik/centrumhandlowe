using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace Centrum.Model
{
    public class CentrumContext : DbContext
    {
        public CentrumContext()
        {
            
        }

        public CentrumContext(DbContextOptions<CentrumContext> options) : base(options)
        {
            
        }
        
        public virtual Microsoft.EntityFrameworkCore.DbSet<Obiekt> Obiekt { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=CentrumHandlowe;User Id=sa;Password=");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Obiekt>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Nazwa).HasColumnName("Nazwa");
                entity.Property(e => e.nazwaTechniczna).HasColumnName("nazwaTechniczna");
                entity.Property(e => e.Powierzchnia).HasColumnName("Powierzchnia");
                entity.Property(e => e.typObiektu).HasColumnName("typObiektu");
                entity.Property(e => e.IloscMiejsc).HasColumnName("IloscMiejsc");
                entity.Property(e => e.typDzialalnosci).HasColumnName("typDzialalnosci");
                entity.Property(e => e.powierzchniaWynajmu).HasColumnName("powierzchniaWynajmu");
                entity.Property(e => e.cenaWynajmu).HasColumnType("cenaWynajmu");
                entity.Property(e => e.dataPoczatkuWynajmu).HasColumnName("dataPoczatkuWynajmu");
                entity.Property(e => e.dataZakonczeniaWynajmu).HasColumnName("dataZakonczeniaWynajmu");
            });
        }

        
    }
}