using System;

namespace Centrum.Model
{
    public class Obiekt
    {
        public int ID { get; set; }
        public string? Nazwa { get; set; }
        public string? nazwaTechniczna { get; set; }
        public double? Powierzchnia { get; set; }
        public Typ_obiektu? typObiektu { get; set; }
        public int? IloscMiejsc { get; set; }
        public Typ_dzialalnosci? typDzialalnosci { get; set; }
        public double? powierzchniaWynajmu { get; set; }
        public decimal? cenaWynajmu { get; set; }
        public DateTime? dataPoczatkuWynajmu { get; set; }
        public DateTime?dataZakonczeniaWynajmu { get; set; }
    }
}