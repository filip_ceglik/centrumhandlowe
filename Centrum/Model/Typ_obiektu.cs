namespace Centrum.Model
{
    public enum Typ_obiektu
    {
        Poziom,
        Korytarz,
        PomieszczenieDoWynajecia,
        Pomieszczenie,
        PomieszczenieTechniczne,
        Stand,
        Parking,
        Inny
    }
}