﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Centrum.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Obiekt",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nazwa = table.Column<string>(nullable: true),
                    nazwaTechniczna = table.Column<string>(nullable: true),
                    Powierzchnia = table.Column<double>(nullable: true),
                    typObiektu = table.Column<int>(nullable: true),
                    IloscMiejsc = table.Column<int>(nullable: true),
                    typDzialalnosci = table.Column<int>(nullable: true),
                    powierzchniaWynajmu = table.Column<double>(nullable: true),
                    cenaWynajmu = table.Column<decimal>(type: "cenaWynajmu", nullable: true),
                    dataPoczatkuWynajmu = table.Column<DateTime>(nullable: true),
                    dataZakonczeniaWynajmu = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Obiekt", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Obiekt");
        }
    }
}
