﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Centrum.Model;
using Microsoft.EntityFrameworkCore;

namespace Centrum
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await using var context = new CentrumContext();
            //context.
            
            #region zad2
            
            var Jysk = new Obiekt()
            {
                cenaWynajmu = 2500,
                dataPoczatkuWynajmu = new DateTime(2018,07,20),
                dataZakonczeniaWynajmu = new DateTime(2021,12,20),
                Nazwa = "Jysk",
                Powierzchnia = 78,
                typObiektu = Typ_obiektu.Stand,
                typDzialalnosci = Typ_dzialalnosci.Sprzedaz
            };
            var Empik = new Obiekt()
            {
                cenaWynajmu = 10000,
                dataPoczatkuWynajmu = new DateTime(2015,03,12),
                dataZakonczeniaWynajmu = new DateTime(2022,11,2),
                Nazwa = "Empik",
                Powierzchnia = 1200,
                typObiektu = Typ_obiektu.Stand,
                typDzialalnosci = Typ_dzialalnosci.Sprzedaz
            };
            
            var pietroDrugie = new Obiekt()
            {
                cenaWynajmu = 100000,
                Nazwa = "Pietro drugie",
                nazwaTechniczna = "V2",
                Powierzchnia = 15000,
                typObiektu = Typ_obiektu.Poziom,
            };
            
            var korytarz = new Obiekt()
            {
                Nazwa = "Korytarz wschodni",
                nazwaTechniczna = "K1",
                Powierzchnia = 9500,
                typObiektu = Typ_obiektu.Korytarz,
            };
            var sklepSpozywczy = new Obiekt()
            {
                cenaWynajmu = 1300,
                Nazwa = "Tesco",
                Powierzchnia = 1500,
                typObiektu = Typ_obiektu.Stand,
            };
            
            var wolnySklep = new Obiekt()
            {
                cenaWynajmu = 3000,
                dataPoczatkuWynajmu = new DateTime(2020,06,17),
                nazwaTechniczna = "C8",
                Powierzchnia = 4000,
                powierzchniaWynajmu = 4000,
                typObiektu = Typ_obiektu.PomieszczenieDoWynajecia
            };
            
            var parkingPoludniowy = new Obiekt()
            {
                IloscMiejsc = 4000,
                nazwaTechniczna = "P2",
                Powierzchnia = 20000,
                typObiektu = Typ_obiektu.Parking
            };
            
            var toaleta = new Obiekt()
            {
                nazwaTechniczna = "P6",
                Powierzchnia = 30,
                typObiektu = Typ_obiektu.Pomieszczenie
            };
            
            var lunchroom2 = new Obiekt()
            {
                nazwaTechniczna = "P7",
                Powierzchnia = 30,
                typObiektu = Typ_obiektu.PomieszczenieTechniczne
            };
            
            var bawilandia = new Obiekt()
            {
                Nazwa = "Bawilandia",
                Powierzchnia = 20000,
                typObiektu = Typ_obiektu.Inny,
                cenaWynajmu = 40000,
                dataPoczatkuWynajmu = new DateTime(2019,03,02),
                dataZakonczeniaWynajmu = new DateTime(2022,10,12),
                typDzialalnosci = Typ_dzialalnosci.Uslugi
            };
            
            var bershka = new Obiekt()
            {
                Nazwa = "Bershka",
                Powierzchnia = 2000,
                typObiektu = Typ_obiektu.Stand,
                cenaWynajmu = 4000,
                dataPoczatkuWynajmu = new DateTime(2017,03,02),
                dataZakonczeniaWynajmu = new DateTime(2023,1,12),
                typDzialalnosci = Typ_dzialalnosci.Sprzedaz
            };
            
            var kfc = new Obiekt()
            {
                Nazwa = "KFC",
                Powierzchnia = 20000,
                typObiektu = Typ_obiektu.Stand,
                cenaWynajmu = 40000,
                dataPoczatkuWynajmu = new DateTime(2018,03,02),
                dataZakonczeniaWynajmu = new DateTime(2022,12,12),
                typDzialalnosci = Typ_dzialalnosci.Sprzedaz
            };

            List<Obiekt> lst = new List<Obiekt>();
            lst.Add(Jysk);
            lst.Add(Empik);
            lst.Add(pietroDrugie);
            lst.Add(korytarz);
            lst.Add(sklepSpozywczy);
            lst.Add(wolnySklep);
            lst.Add(parkingPoludniowy);
            lst.Add(toaleta);
            lst.Add(lunchroom2);
            lst.Add(bawilandia);
            lst.Add(bershka);
            lst.Add(kfc);

            await context.AddRangeAsync(lst);
            
            #endregion

            #region zad3
            
            var result = context.Obiekt.SingleOrDefault(x => x.nazwaTechniczna == "A3");
            result.dataZakonczeniaWynajmu = new DateTime(2024,4,15);
            #endregion

            
            //await context.SaveChangesAsync();

            #region zad1
            
            var one = context.Obiekt.Where(x => x.typObiektu == Typ_obiektu.Stand || x.typObiektu ==Typ_obiektu.Pomieszczenie).ToListAsync();
            
            #endregion

            #region zad4

            context.RemoveRange(lst);
            await context.SaveChangesAsync();

            #endregion
        }
    }
}